<?php

require('pushy.php');

class User extends CI_Controller {
  
  public function get_private_messages() {
    $myUserID = intval($this->input->post('my_user_id'));
    $opponentUserID = intval($this->input->post('opponent_user_id'));
    $start = intval($this->input->post('start'));
    $length = intval($this->input->post('length'));
    $messages = $this->db->query("SELECT * FROM private_messages WHERE (sender_id=" . $myUserID . " AND receiver_id=" . $opponentUserID . ") OR (sender_id=" . $opponentUserID . " AND receiver_id=" . $myUserID . ") ORDER BY date DESC LIMIT " . $start . "," . $length)->result_array();
    for ($i=0; $i<sizeof($messages); $i++) {
      $messages[$i]['sender_name'] = $this->db->get_where('users', array(
        'id' => intval($messages[$i]['sender_id'])
      ))->row_array()['name'];
      $messages[$i]['receiver_name'] = $this->db->get_where('users', array(
        'id' => intval($messages[$i]['receiver_id'])
      ))->row_array()['name'];
    }
    echo json_encode($messages);
  }
  
  public function send_image_to_user() {
    $senderID = intval($this->input->post('sender_id'));
    $receiverID = intval($this->input->post('receiver_id'));
    $blockedUsers = $this->db->query("SELECT * FROM `blocked_users` WHERE (`blocked_user_id`=" . $senderID . " AND `user_id`=" . $receiverID . ") OR (`blocked_user_id`=" . $receiverID . " AND `user_id`=" . $senderID . ")")->result_array();
    if (sizeof($blockedUsers) > 0) {
      return;
    }
    $date = $this->input->post('date');
    $config = array(
        'upload_path' => './userdata',
        'allowed_types' => "gif|jpg|png|jpeg",
        'overwrite' => TRUE,
        'max_size' => "2048000"
        );
        $this->load->library('upload', $config);
        if($this->upload->do_upload('file')) { 
          $this->db->insert('private_messages', array(
            'sender_id' => $senderID,
            'receiver_id' => $receiverID,
            'message' => "",
            'image' => $this->upload->data()['file_name'],
            'date' => $date
          ));
          $lastID = intval($this->db->insert_id());
          $receiverToken = $this->db->get_where('users', array(
            'id' => $receiverID
          ))->row_array()['pushy_token'];
          PushyAPI::send_message($receiverToken, 'Pesan baru', "Gambar diterima", array(
            'message_id' => $lastID
          ));
          $row = $this->db->get_where('private_messages', array(
            'id' => $lastID
          ))->row_array();
          $row['sender_name'] = $this->db->get_where('users', array(
            'id' => $senderID
          ))->row_array()['name'];
          $row['receiver_name'] = $this->db->get_where('users', array(
            'id' => $receiverID
          ))->row_array()['name'];
          echo json_encode($row);
        }
  }
  
  public function send_message_to_user() {
    $senderID = intval($this->input->post('sender_id'));
    $receiverID = intval($this->input->post('receiver_id'));
    $blockedUsers = $this->db->query("SELECT * FROM `blocked_users` WHERE (`blocked_user_id`=" . $senderID . " AND `user_id`=" . $receiverID . ") OR (`blocked_user_id`=" . $receiverID . " AND `user_id`=" . $senderID . ")")->result_array();
    if (sizeof($blockedUsers) > 0) {
      return;
    }
    $message = $this->input->post('message');
    $shortMessage = $message;
    if (strlen($message) > 60) {
      $shortMessage = substr($message, 0, 60);
    }
    $date = $this->input->post('date');
    $this->db->insert('private_messages', array(
      'sender_id' => $senderID,
      'receiver_id' => $receiverID,
      'message' => $message,
      'date' => $date
    ));
    $lastID = intval($this->db->insert_id());
    $receiverToken = $this->db->get_where('users', array(
      'id' => $receiverID
    ))->row_array()['pushy_token'];
    PushyAPI::send_message($receiverToken, 'Pesan baru', $shortMessage, array(
      'message_id' => $lastID
    ));
    $messageObj = $this->db->get_where('private_messages', array(
      'id' => $lastID
    ))->row_array();
    $messageObj['sender_name'] = $this->db->get_where('users', array(
      'id' => $senderID
    ))->row_array()['name'];
    echo json_encode($messageObj);
  }
  
  public function delete_video() {
    $id = intval($this->input->post('id'));
    $videoName = $this->db->get_where('videos', array(
      'id' => $id
    ))->row_array()['video_path'];
    unlink('./userdata/' . $videoName);
    $this->db->where('id', $id);
    $this->db->delete('videos');
  }
  
  public function get_contacts() {
    $start = intval($this->input->post('start'));
    $length = intval($this->input->post('length'));
    $this->db->order_by('name', 'ASC');
    $this->db->limit($length, $start);
    echo json_encode($this->db->get('contacts')->result_array());
  }
  
  public function get_latest_videos() {
    $start = intval($this->input->post('start'));
    $length = intval($this->input->post('length'));
    $this->db->limit($length, $start);
    $this->db->order_by('date', 'DESC');
    $videos = $this->db->get('videos')->result_array();
    for ($i=0; $i<sizeof($videos); $i++) {
      $videos[$i]['uploader'] = $this->db->get_where('users', array(
        'id' => intval($videos[$i]['uploader_id'])
      ))->row_array()['name'];
    }
    echo json_encode($videos);
  }
  
  public function get_most_liked_videos() {
    $start = intval($this->input->post('start'));
    $length = intval($this->input->post('length'));
    $this->db->limit($length, $start);
    $this->db->order_by('likes', 'DESC');
    $videos = $this->db->get('videos')->result_array();
    for ($i=0; $i<sizeof($videos); $i++) {
      $videos[$i]['uploader'] = $this->db->get_where('users', array(
        'id' => intval($videos[$i]['uploader_id'])
      ))->row_array()['name'];
    }
    echo json_encode($videos);
  }
  
  public function get_most_viewed_videos() {
    $start = intval($this->input->post('start'));
    $length = intval($this->input->post('length'));
    $this->db->limit($length, $start);
    $this->db->order_by('viewers', 'DESC');
    $videos = $this->db->get('videos')->result_array();
    for ($i=0; $i<sizeof($videos); $i++) {
      $videos[$i]['uploader'] = $this->db->get_where('users', array(
        'id' => intval($videos[$i]['uploader_id'])
      ))->row_array()['name'];
    }
    echo json_encode($videos);
  }
  
  public function get_featured_videos() {
    $start = intval($this->input->post('start'));
    $length = intval($this->input->post('length'));
    $this->db->limit($length, $start);
    $this->db->where('featured', 1);
    $videos = $this->db->get('videos')->result_array();
    for ($i=0; $i<sizeof($videos); $i++) {
      $videos[$i]['uploader'] = $this->db->get_where('users', array(
        'id' => intval($videos[$i]['uploader_id'])
      ))->row_array()['name'];
    }
    echo json_encode($videos);
  }
  
  public function get_nearest_videos() {
    $start = intval($this->input->post('start'));
    $length = intval($this->input->post('length'));
    $lat = doubleval($this->input->post('lat'));
    $lng = doubleval($this->input->post('lng'));
    $this->db->limit($length, $start);
    $videos = $this->db->query('SELECT *, SQRT(
    POW(69.1 * (lat - ' . $lat . '), 2) +
    POW(69.1 * (' . $lng . '- lng) * COS(lat / 57.3), 2)) AS distance
FROM videos HAVING distance < 25 ORDER BY distance;')->result_array();
    for ($i=0; $i<sizeof($videos); $i++) {
      $videos[$i]['uploader'] = $this->db->get_where('users', array(
        'id' => intval($videos[$i]['uploader_id'])
      ))->row_array()['name'];
    }
    echo json_encode($videos);
  }
  
  public function get_favorite_videos() {
    $userID = intval($this->input->post('user_id'));
    $start = intval($this->input->post('start'));
    $length = intval($this->input->post('length'));
    $this->db->limit($length, $start);
    $this->db->where('user_id', $userID);
    $videos = $this->db->get('videos')->result_array();
    for ($i=0; $i<sizeof($videos); $i++) {
      $videos[$i]['uploader'] = $this->db->get_where('users', array(
        'id' => intval($videos[$i]['uploader_id'])
      ))->row_array()['name'];
    }
    echo json_encode($videos);
  }
  
  public function get_my_videos() {
    $userID = intval($this->input->post('user_id'));
    $start = intval($this->input->post('start'));
    $length = intval($this->input->post('length'));
    $this->db->limit($length, $start);
    $this->db->where('uploader_id', $userID);
    $videos = $this->db->get('videos')->result_array();
    for ($i=0; $i<sizeof($videos); $i++) {
      $videos[$i]['uploader'] = $this->db->get_where('users', array(
        'id' => intval($videos[$i]['uploader_id'])
      ))->row_array()['name'];
    }
    echo json_encode($videos);
  }
  
  /*public function send_message_to_user() {
    $userID = intval($this->input->post('user_id'));
    $messageID = intval($this->input->post('message_id'));
    $message = $this->db->get_where('messages', array(
      'id' => $messageID
    ))->row_array();
    $shortMessage = $message['message'];
    if (strlen($message['message']) > 60) {
      $shortMessage = substr($message['message'], 0, 60);
    }
    $token = $this->db->get_where('users', array(
      'id' => $userID
    ))->row_array()['pushy_token'];
    PushyAPI::send_message($token, 'Pesan baru', $shortMessage, array(
      'sender_id' => $userID,
      'message_id' => $messageID,
      ''
    ));
  }*/
  
  public function update_pushy_token() {
    $userID = intval($this->input->post('user_id'));
    $token = $this->input->post('token');
    $this->db->where('id', $userID);
    $this->db->update('users', array(
      'pushy_token' => $token
    ));
  }
  
  public function update_fcm_id() {
    $userID = intval($this->input->post('user_id'));
    $fcmID = $this->input->post('fcm_id');
    $this->db->where('id', $userID);
    $this->db->update('users', array(
      'fcm_id' => $fcmID
    ));
  }
  
  public function send_image() {
    $senderID = intval($this->input->post('sender_id'));
    $date = $this->input->post('date');
    $config = array(
        'upload_path' => './userdata',
        'allowed_types' => "gif|jpg|png|jpeg",
        'overwrite' => TRUE,
        'max_size' => "2048000"
        );
        $this->load->library('upload', $config);
        if($this->upload->do_upload('file')) { 
          $this->db->insert('messages', array(
            'sender_id' => $senderID,
            'message' => '',
            'image' => $this->upload->data()['file_name'],
            'date' => $date
          ));
          $lastID = intval($this->db->insert_id());
          $users = $this->db->get('users')->result_array();
          /*for ($i=0; $i<sizeof($users); $i++) {
            $user = $users[$i];
            if (intval($user['id']) == $senderID) {
              continue;
            }
            PushyAPI::send_message($user['pushy_token'], 'Pesan baru', $shortMessage);
          }*/
          $row = $this->db->get_where('messages', array(
            'id' => $lastID
          ))->row_array();
          $row['name'] = $this->db->get_where('users', array(
            'id' => $senderID
          ))->row_array()['name'];
          echo json_encode($row);
        }
  }
  
  public function send_message() {
    $message = $this->input->post('message');
    $shortMessage = $message;
    if (strlen($message) > 60) {
      $shortMessage = substr($message, 0, 60);
    }
    $senderID = intval($this->input->post('sender_id'));
    $date = $this->input->post('date');
    $this->db->insert('messages', array(
      'sender_id' => $senderID,
      'message' => $message,
      'date' => $date
    ));
    $lastID = intval($this->db->insert_id());
    $users = $this->db->get('users')->result_array();
    
    /*for ($i=0; $i<sizeof($users); $i++) {
      $user = $users[$i];
      if (intval($user['id']) == $senderID) {
        continue;
      }
      PushyAPI::send_message($user['pushy_token'], 'Pesan baru', $shortMessage, array(
        'message_id' => "" . $lastID
      ));
    }*/
    $row = $this->db->get_where('messages', array(
            'id' => $lastID
          ))->row_array();
    $row['name'] = $this->db->get_where('users', array(
      'id' => $senderID
    ))->row_array()['name'];
    echo json_encode($row);
  }
  
  public function get_messages() {
    $start = intval($this->input->post('start'));
    $length = intval($this->input->post('length'));
    $this->db->limit($length, $start);
    $this->db->order_by('date', 'DESC');
    $messages = $this->db->get('messages')->result_array();
    for ($i=0; $i<sizeof($messages); $i++) {
      $messages[$i]['name'] = $this->db->get_where('users', array(
        'id' => intval($messages[$i]['sender_id'])
      ))->row_array()['name'];
    }
    echo json_encode($messages);
  }
  
  public function signup() {
    $name = $this->input->post('name');
    $description = $this->input->post('description');
    $birthYear = intval($this->input->post('birth_year'));
    $gender = intval($this->input->post('gender'));
    $allowComments = intval($this->input->post('allow_comments'));
    $allowPrivateChats = intval($this->input->post('allow_private_chats'));
    $receiveAlerts = intval($this->input->post('receive_alerts'));
    $role = intval($this->input->post('role'));
    $config = array(
        'upload_path' => './userdata',
        'allowed_types' => "gif|jpg|png|jpeg",
        'overwrite' => TRUE,
        'max_size' => "2048000"
        );
        $this->load->library('upload', $config);
        if($this->upload->do_upload('file')) { 
            $this->db->insert('users', array(
            'name' => $name,
            'description' => $description,
            'birth_year' => $birthYear,
            'gender' => $gender,
            'photo' => $this->upload->data('file_name'),
            'allow_comments' => $allowComments,
            'allow_private_chats' => $allowPrivateChats,
            'receive_alerts' => $receiveAlerts,
            'role' => $role
          ));
        }
        echo json_encode(
          array(
            'response_code' => 1,
            'data' => array(
              'user_id' => $this->db->insert_id()
            )
           )
        );
  }
}